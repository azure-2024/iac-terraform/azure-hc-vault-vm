terraform {

  required_version = ">= 1.2"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.9"
    }

    random = {
      source  = "hashicorp/random"
      version = ">=3.4"
    }
  }
}

provider "azurerm" {
  features {}
}
