terraform {
  backend "azurerm" {
    resource_group_name   = "tfstate-resources"
    storage_account_name  = "tfstate8yypka49gnqt"
    container_name        = "tfstate"
    key                   = "terraform-vault.tfstate"
  }
}
