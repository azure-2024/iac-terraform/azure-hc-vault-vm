locals {
  ip_whitelist = jsondecode(file("${path.module}/ip_whitelist.json"))
}

resource "azurerm_network_security_group" "nsg" {
  name                = "vault-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_network_security_rule" "allow_vault_ipv4" {
  name                        = "allow-vault-access-ipv4"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "8200"
  source_address_prefixes     = local.ip_whitelist.ipv4
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "allow_vault_ipv6" {
  name                        = "allow-vault-access-ipv6"
  priority                    = 110
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "8200"
  source_address_prefix       = local.ip_whitelist.ipv6[0]
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = azurerm_subnet.docker.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_container_group" "docker" {
  name = "vault-docker"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  ip_address_type     = "Public"
  os_type             = "Linux"

  container {
    name   = "vault"
    image  = "hashicorp/vault:latest"
    cpu    = "1"
    memory = "2"
    commands = ["/bin/sh", "-c", "vault server -config=/etc/vault/config/config.hcl"]

    ports {
      port     = 8200
      protocol = "TCP"
    }

    volume {
      name       = "vault"
      mount_path = "/etc/vault"
      read_only  = false
      share_name = azurerm_storage_share.vault.name
      storage_account_name = azurerm_storage_account.vault.name
      storage_account_key  = azurerm_storage_account.vault.primary_access_key
    }
  }
}
